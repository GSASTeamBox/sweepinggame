//Effects
var EFFECT_DEPTH = -1;
var effectList = [];

var EffectsLayer = cc.Layer.extend({
    //spriteSheet:null,
    poofingAction:null,
    //sprite:null,
    ctor:function () {
        this._super();
        this.scheduleUpdate();
        
    },

    
    destroySelf:function( inputTarget ) {
        this.removeChild( inputTarget );
    },
    
    createPoof:function( xLoc, yLoc) {
        //this._super();
        var spriteSheet;
        // create sprite sheet
        cc.spriteFrameCache.addSpriteFrames(res.DustPuff_plist);
        spriteSheet = cc.SpriteBatchNode.create(res.DustPuff_png);
        this.addChild(spriteSheet, EFFECT_DEPTH);


        // init poofingAction
        var animFrames = [];
        for (var i = 1; i <= 6; i++) {
            var str = "NewPoof" + i + ".png";
            var frame = cc.spriteFrameCache.getSpriteFrame(str);
            animFrames.push(frame);
        }

        var animation = cc.Animation.create(animFrames, 0.1);
        this.poofingAction = cc.Animate.create(animation);
        var sprite = cc.Sprite.create("#NewPoof1.png");
        sprite.attr({x:50, y:50});
        
        var seq = cc.Sequence.create( this.poofingAction, cc.CallFunc.create( this.destroySelf, spriteSheet) );
        //var seq = cc.Sequence.create( this.poofingAction );
        
        sprite.runAction(seq);
        spriteSheet.addChild(sprite, EFFECT_DEPTH);
        sprite.setPosition( xLoc, yLoc );
        //this.addChild( sprite );
        
        return spriteSheet;
    }
    
    
    
});