
var sizeNeeded = 5;
var trst = 5;
var hasStarted = 0;
var maxNumberOfDust = 5;

var introMenu;

var Position = [sizeNeeded];
var HelloWorldLayer = cc.Layer.extend({
    sprite:null,
    ctor:function(){
        this._super();
    },
    init:function(){
        console.log("stuff");
        this._super();
        
        var winsize = cc.director.getWinSize();

        introMenu = new cc.Sprite(res.intro1_png);
        introMenu.attr({
            x: winsize.width / 2,
            y: winsize.height / 2,
            scale: 1
            //rotation: 180
        });
        this.addChild(introMenu, 0);
        var centerpos = cc.p(winsize.width / 2, winsize.height / 2);
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan
        }, this)        
        this.scheduleUpdate();

    },
    loadGame:function(){
            console.log("stuff");
        var myWorld;        
        var winsize = cc.director.getWinSize();

        myWorld = new cc.Sprite(res.HelloWorld_png);
        myWorld.attr({
            x: winsize.width / 2,
            y: winsize.height / 2,
            scale: 1,
            rotation: 180
        });
        this.addChild(myWorld, 0);
        var centerpos = cc.p(winsize.width / 2, winsize.height / 2);
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan
        }, this)
        
    },
    onTouchBegan:function(touch, event) {
        if(hasStarted == 0){
            hasStarted = 1;

        }else{
            trst = 50;
        }
        return true;
    },
    check:function () {
        var spriteA = new Dust();//cc.Sprite(res.Hat_png);//"res/fall1.png" );//res.Hat_png);
        Position.push(spriteA.position);
        this.addChild(spriteA);
    },
    createDust:function(numberOfDustToAdd, dustHasBeenDestroyed, numbersOfGroups){
        if(dustHasBeenDestroyed == 1){
            sizeNeeded+= maxNumberOfDust;
            maxNumberOfDust+=5;
        }else{
            sizeNeeded+=numberOfDustToAdd;
        }
        
    },
    update:function (dt) {
    if(hasStarted == 0){
    }else if(hasStarted == 1){
        this.loadGame();
        introMenu.getParent().removeChild(introMenu,true);
        hasStarted = 2;
     }else{
        if(trst == 50){
            this.createDust(5,0,0);
            trst = 49;
        }
        var sizeOfDust = (Math.floor(Math.random()*200)+1);

       if(sizeOfDust < 10 && sizeNeeded > 0){
            sizeNeeded--;

          
          var spriteA = new Dust();//cc.Sprite(res.Hat_png);//"res/fall1.png" );//res.Hat_png);
    
            Position.push(spriteA.position);
            spriteA.hidden = true;
             this.addChild(spriteA);
            spriteA.hidden = false;

             
       }else{

       }
    }
    }
});


var MainScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        /*
        var layer = new HelloWorldLayer();
                layer.init();

        this.addChild(layer);
        */
        var effectLayer = new EffectsLayer();
        var backgroundLayer = new BackgroundLayer();
        var flashLayer = new FlashLayer();
        var layer3 = new GameplayLayer( effectLayer, backgroundLayer, flashLayer );
        var titleLayer = new TitleLayer();
        
        this.addChild(backgroundLayer);
        this.addChild(layer3);
        this.addChild(effectLayer);
        this.addChild(flashLayer);
        this.addChild(titleLayer);



        cc.audioEngine.playMusic(res.sweep_sweep_wma, true);
        cc.audioEngine.setMusicVolume(.2);
        cc.audioEngine.setEffectsVolume(.2);


        var flashLayer = new FlashLayer();

        
        this.addChild(flashLayer);
        

    }
});

