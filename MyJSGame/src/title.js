//Manage backgrounds

var hasStarted = 0;

var TitleLayer = cc.Layer.extend({
    intoMenu:null,
    ctor:function () {

        this._super();
        this.init();

    },
    init:function(){
        var winsize = cc.director.getWinSize();

        this.introMenu = new cc.Sprite(res.intro_png);
        this.introMenu.attr({
            x: winsize.width / 2,
            y: winsize.height / 2,
            scale: 1
            //rotation: 180
        });
        this.addChild(this.introMenu, 100);
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan
        }, this)
        this.scheduleUpdate();

    },
    onTouchBegan:function(touch, event) {
        if(hasStarted == 0){
            hasStarted = 1;

        }
        return true;
    },
    update:function (dt) {
        if(hasStarted == 1){
            hasStarted = 2;
            this.removeChild( this.introMenu );

        }
    }
    

});
