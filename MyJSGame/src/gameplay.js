var KEY_UP_CODE = 38;
var KEY_DOWN_CODE = 40;
var KEY_W_CODE = 87;
var KEY_S_CODE = 83;
var INDEX_UP = 0;
var INDEX_DOWN = 1;
var INDEX_LEFT = 2;
var INDEX_RIGHT = 3;
var DUST_DEPTH = -1;
var PAN_DEPTH = -10;
var BROOM_DEPTH = 10;
var SPAWN_DUST_GROUP_AMOUNT = 5
var NUM_ROUNDS_BEFORE_CHANGE = 4;
var inputArray = [false, false, false, false];
var dustList = [];
var broom;
var soundID;


Math.radians = function( degrees ) {
    return degrees * Math.PI / 180;
}

var Dust = cc.Sprite.extend({
    edgeBoundry: 64,
    hitBox:null,
    hitBoxSize:8,
    currentX:0,
    currentY:0,
    attachOffsetX:0,
    attachOffsetY:0,
    attached:false,
    attachTarget:null,
    spriteChoice:[res.dust0_png, res.dust1_png, res.dust2_png],
    ctor:function( xPos, yPos) {
        this._super( this.spriteChoice[ Math.floor( Math.random() * this.spriteChoice.length ) ]);
        this.setScale(1);
        this.setPosition( xPos, yPos);
        this.setRotation( Math.random() * 360 );
        this.currentX = xPos;
        this.currentY = yPos;
        this.hitBox = new CollRect( xPos, yPos, this.hitBoxSize, this.hitBoxSize );
        this.scheduleUpdate();//Do this so the update method gets called
    },

    update:function(dt) {
        if ( this.attached && this.attachTarget != null ) {
            this.currentX = this.attachTarget._x + this.attachOffsetX;
            this.currentY = this.attachTarget._y + this.attachOffsetY;
            this.setPosition( this.currentX, this.currentY );
            
            this.hitBox.setPos( this.currentX, this.currentY );
        }

    }

});

//Control dust spawning
function DustController() {
    this.groupRadiusMax = 128;
    this.groupDensity = 15;
    this.minDust = 10;
    this.edgeBoundry = 128;
    this.dustList = [];
    this.burstSpawnDust = function( numberOfGroups ){
        for ( var i = 0; i < numberOfGroups; ++i ) {
            var tempX = (Math.floor(Math.random() * (cc.winSize.width - this.edgeBoundry * 2 )) + this.edgeBoundry);
            var tempY = (Math.floor(Math.random() * (cc.winSize.height - this.edgeBoundry * 2 )) + this.edgeBoundry - 16);
            var numDust = this.minDust + Math.floor( Math.random() * this.groupDensity );
            
            for ( var j = 0; j < numDust; ++j ) {
                var angle = Math.random() * 360;
                var dist = Math.random() * this.groupRadiusMax;
                
                var spawnX = dist * Math.cos( Math.radians( angle ) );
                var spawnY = dist * Math.sin( Math.radians( angle ) );
                var newDust = new Dust( tempX + spawnX, tempY + spawnY );
                this.dustList.push( newDust );
            }
            
            
            
        }
    }
}; 


var DustPan = cc.Sprite.extend({
    dustFill:0,
    prevFill:0,
    dustFillIncrement:1,
    hitCircle:null,
    hitRadius:120,
    ctor:function() {
        //this._super(res.Dustpan_png);
        this._super();
        this.setTexture( res.Dustpan_png );
        this.setScale(1);
        this.setPosition( 50, cc.winSize.height / 2 );
        this.hitCircle = new CollCircle( this.x, this.y, this.hitRadius );
        
        this.scheduleUpdate();//Do this so the update method gets called
    },

    update:function(dt) {
        if ( inputArray[ INDEX_UP ] ) {
            this.y += 100 * dt;
        }
        if ( inputArray[ INDEX_DOWN ] ) {
            this.y -= 100 * dt;
        }
        this.hitCircle.setPos( this.x, this.y );
        this.updateImage();
    },
    
    updateImage:function() {
        var numFills = Math.floor( this.dustFill / this.dustFillIncrement );
        
        if ( numFills > this.prevFill ) {
            this.prevFill = numFills;
            switch( numFills ) {
                case 0:
                    this.setTexture( res.Pan0_png ); break;
                case 1:
                    this.setTexture( res.Pan1_png ); break;
                case 2:
                    this.setTexture( res.Pan2_png ); break;
                case 3:
                    this.setTexture( res.Pan3_png ); break;
                default:
                    this.setTexture( res.Pan4_png ); break;
            }
        }
    }

});

var Broom = cc.Sprite.extend({
    down:false,
    _x:0,
    _y:0,
    z:-10,
    prevX:0,
    prevY:0,
    move:0,
    maxSpeed:200,
    maxPercent:0.95,
    minPercent:0.2,
    currentSpeedPercent:0,
    collCircle:null,
    hitCircleSize:64,
    hitCircleOffset:-64,
    ctor:function(){
        cc.spriteFrameCache.addSpriteFrames(res.Broom_plist);
        this._super(res.BroomIdle_png);
        //this.initWithSpriteFrame(res.BroomIdle_png);
        this.scheduleUpdate();

        //Collisions
        this.collCircle = new CollCircle( 0, 0, this.hitCircleSize );
        
        //Animation
        var downAnim = [];
        var frame = cc.spriteFrameCache.getSpriteFrame("BroomGround.png");
        downAnim.push(frame);

        var downAnimation = cc.Animation.create(downAnim, .1);
        this.downAction = cc.sequence(cc.Animate.create(downAnimation));

        var IdleAnim = [];
        for(var i = 1; i <= 5; i++){
            var frame = cc.spriteFrameCache.getSpriteFrame("BroomRiseL" + i + ".png");
            IdleAnim.push(frame);
        }
        //var frame = ;
        IdleAnim.push(cc.spriteFrameCache.getSpriteFrame("BroomIdle.png"));

        var IdleAnimation = cc.Animation.create(IdleAnim, .05);
        this.upLeftAction = cc.sequence(cc.Animate.create(IdleAnimation));
        
        var upRightAnim = [];
        for(var i = 1; i <= 5; i++){
            var frame = cc.spriteFrameCache.getSpriteFrame("BroomRiseR" + i + ".png");
            upRightAnim.push(frame);
        }
        //var frame = ;
        upRightAnim.push(cc.spriteFrameCache.getSpriteFrame("BroomIdle.png"));

        var upRightAnimation = cc.Animation.create(upRightAnim, .05);
        this.upRightAction = cc.sequence(cc.Animate.create(upRightAnimation));;

        var LeftAnim = [];
        for(var i = 1; i <= 2; i++){
            var frame = cc.spriteFrameCache.getSpriteFrame("BroomSweepL" + i + ".png");
            LeftAnim.push(frame);
        }

        var LeftAnimation = cc.Animation.create(LeftAnim, .05);
        this.LeftAction = cc.sequence(cc.Animate.create(LeftAnimation));

        var RightAnim = [];
        for(var i = 1; i <= 2; i++){
            var frame = cc.spriteFrameCache.getSpriteFrame("BroomSweepR" + i + ".png");
            RightAnim.push(frame);
        }

        var RightAnimation = cc.Animation.create(RightAnim, .05);
        this.RightAction = cc.sequence(cc.Animate.create(RightAnimation));

    },
    update:function(dt){

        //if((this.x != this._x || this.y != this._y) && this.down) {
        //    cc.audioEngine.playEffect(res.broom_move_wav);
        //}

        this.prevX = this.x;
        this.prevY = this.y;
   
        if(this.x > this._x && this.down)
        {
            if(move != 1)
                soundID = cc.audioEngine.playEffect(res.broom_move_wav);
            move = 1;
            this.runAction(this.LeftAction);
        }
        else if(this.x < this._x && this.down)
        {
            if(move != 2)
                soundID = cc.audioEngine.playEffect(res.broom_move_wav);
            move = 2;
            this.runAction(this.RightAction);
        }
        else
            move = 0;
        //if(this.down && Math.abs(this.x - this._x) < .2)
            //this.runAction(this.downAction);
        this.setPosition(cc.p(this._x,this._y));
        this.collCircle.setPos( this._x, this._y + this.hitCircleOffset );
        
        if ( this.down ) {
            this.setCurrentSpeed();
        }
    },
    
    setCurrentSpeed:function() {
        var currentSpeed = Math.sqrt( Math.pow(( this._x - this.prevX ), 2 ) + Math.pow(( this._y - this.prevY ), 2 ) );
        var speedPercent = currentSpeed / this.maxSpeed;
        speedPercent = Math.min( Math.max( speedPercent, this.minPercent ), this.maxPercent );
        this.currentSpeedPercent = speedPercent;
    }
    
});

var GameplayLayer = cc.Layer.extend({
    dustpan:null,
    broom:null,
    spriteSheet:null,
    inAirAction:null,
    sprite:null,
    dustManager:null,
    globalEffectLayer:null,
    globalBackgroundLayer:null,
    globalFlashLayer:null,
    ctor:function ( effectLayer, backgroundLayer, flashLayer ) {

        this._super();
        this.dustpan = new DustPan();
        this.addChild( this.dustpan, PAN_DEPTH );
        //this.dustpan.inputArray[0] = true;

        this.broom = new Broom();
        var broomRef = this.broom;
        //broom.sprite = cc.Sprite.create("#BroomGround.png");
        this.addChild(this.broom, BROOM_DEPTH);
        //Gameplay dust manager
        this.dustManager = new DustController();
       
        this.globalEffectLayer = effectLayer;
        this.globalBackgroundLayer = backgroundLayer;
        this.globalFlashLayer = flashLayer;
       
        //Input Listener
        cc.eventManager.addListener( {
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function( key, event ) {
                switch( key ) {
                    case KEY_W_CODE:
                        inputArray[INDEX_UP] = true;
                        break;
                    case KEY_S_CODE:
                        inputArray[INDEX_DOWN] = true;
                        break;
                }
                
            }  , 
            onKeyReleased: function( key, event ) {
                switch( key ) {
                    case KEY_W_CODE:
                        inputArray[INDEX_UP] = false;
                        break;
                    case KEY_S_CODE:
                        inputArray[INDEX_DOWN] = false;
                        break;
                }
                
            },   
        }, this);
        cc.eventManager.addListener({
            event:cc.EventListener.MOUSE,
            onMouseMove:function(event){
                //var target = event.getCurrentTarget();
                broomRef._x = event.getLocation().x;
                broomRef._y = event.getLocation().y;
                //this.broom.setPosition(event.getLocation());
            },
            onMouseDown:function(event){
                broomRef.down = true;
                broomRef.runAction(broomRef.downAction);
                cc.audioEngine.playEffect(res.broom_down_wav);
            },
            onMouseUp:function(event){
                broomRef.down = false;


                if(move == 2) {
                    broomRef.runAction(broomRef.upRightAction);
                }
                else if(move == 1) 
                {
                    broomRef.runAction(broomRef.upLeftAction);
                }
                else //move == 0
                    broomRef.runAction(broomRef.upLeftAction)
                cc.audioEngine.stopEffect(soundID);
                cc.audioEngine.playEffect(res.broom_down_wav);

            }  
        }, this);
        
        this.scheduleUpdate();
    },

    
    update:function(dt) {
        if ( this.dustManager.dustList.length <= 0 ) {
            this.spawnDustBurst( SPAWN_DUST_GROUP_AMOUNT );
            this.dustpan.dustFill += this.dustpan.dustFillIncrement;
            this.globalBackgroundLayer.fadeDustCover( 1 / NUM_ROUNDS_BEFORE_CHANGE );
            if ( this.dustpan.dustFill > NUM_ROUNDS_BEFORE_CHANGE ) {
                this.dustpan.dustFill = 0;
                this.dustpan.prevFill = -1;
                this.dustpan.updateImage();
                this.globalBackgroundLayer.setBackground();
                this.globalFlashLayer.doFlash();
                cc.audioEngine.playEffect(res.waterDrop_wav);
            }
        }        
        
        //Collisions
        if ( this.broom.down ) {
            for ( var i = 0; i < this.dustManager.dustList.length; ++ i ) {
                var tempDust = this.dustManager.dustList[ i ];
                var tempRect = tempDust.hitBox;
                var tempCircle = this.broom.collCircle;
                if ( isCircleInRect( tempCircle, tempRect ) ) {
                    //Check for random attachment
                    if ( Math.random() > this.broom.currentSpeedPercent ) {
                        if ( !tempDust.attached ) {
                            tempDust.attachTarget = this.broom;
                            tempDust.attachOffsetX = tempDust.currentX - this.broom._x;
                            tempDust.attachOffsetY = tempDust.currentY - this.broom._y;
                            tempDust.attached = true;
                        }
                    }
                    else {
                        tempDust.attachTarget = null;
                        tempDust.attached = false;
                    }
                }
            }
        }
        else {
            //Release all dust if not broom down
            for ( var i = 0; i < this.dustManager.dustList.length; ++i ) {
                var tempDust = this.dustManager.dustList[ i ];
                tempDust.attachTarget = null;
                tempDust.attached = false;
            }
        }
        
        //Pan Collisions
        for ( var i = 0; i < this.dustManager.dustList.length; ++i ) {
            var tempDust = this.dustManager.dustList[ i ];
            var tempRect = tempDust.hitBox;
            var panCircle = this.dustpan.hitCircle;
            if ( isCircleInRect( panCircle, tempRect ) ) {
                console.log( "dust cleared" );
                this.removeChild( tempDust );
                this.dustManager.dustList.splice( i, 1 );
                this.globalEffectLayer.createPoof( tempDust.currentX, tempDust.currentY );
            }
        }
        
        
    },
    
    spawnDustBurst:function( numGroups ) {
        this.dustManager.burstSpawnDust( numGroups );
        for ( var i = 0; i < this.dustManager.dustList.length; ++ i ) {
            this.addChild( this.dustManager.dustList[ i ], DUST_DEPTH );
        }
        
    }

});
