//Manage backgrounds

var hasStarted = 0;

var BaseBackground = cc.Sprite.extend({
    
    ctor:function( inputImage ) {
        this._super(inputImage);
        //this._super(res.CloseSelected_png);
        this.setScale(1);
        this.setPosition( cc.winSize.width / 2, cc.winSize.height / 2);
    }

});

var CoverBackground = cc.Sprite.extend({
    alpha: 1,
    ctor:function( inputImage ) {
        this._super(inputImage);
        //this._super(res.CloseSelected_png);
        this.setScale(1);
        this.setPosition( cc.winSize.width / 2, cc.winSize.height / 2);
        this.scheduleUpdate();//Do this so the update method gets called
    },
    
    update:function(dt) {
        this.opacity = this.alpha * 255;
    }

});

var BackgroundLayer = cc.Layer.extend({
    mainBackground:null,
    coverBackground:null,
    backgroundList:[res.Background_img1, res.BackgroundCover1_img, res.Background_img2, res.BackgroundCover1_img, res.Background_img3, res.BackgroundCover1_img],
    currentBgIndex:0,
    ctor:function () {

        this._super();
        this.setBackground();

    },

    setBackground:function() {
        if ( this.mainBackground != null ) {
            this.removeChild( this.mainBackground );
        }
        if ( this.coverBackground != null ) {
            this.removeChild( this.coverBackground );
        }
        
        this.mainBackground = new BaseBackground( this.backgroundList[ this.currentBgIndex ] );
        this.coverBackground = new CoverBackground( this.backgroundList[ this.currentBgIndex + 1 ] );
        
        this.addChild( this.mainBackground );
        this.addChild( this.coverBackground );
       
        this.currentBgIndex += 2;
        if ( this.currentBgIndex >= this.backgroundList.length ) {
            this.currentBgIndex = 0;
        }
           
    },
    
    fadeDustCover:function( amount ) {
        if(hasStarted == 2){
        this.coverBackground.alpha -= amount;
        this.coverBackground.alpha = Math.max( this.coverBackground.alpha, 0 );
        }
    },
    
    resetDustCover:function() {
        this.coverBackground.alpha = 1;
    }
    

});
