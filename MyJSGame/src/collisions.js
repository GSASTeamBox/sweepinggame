//Includes rects and circle colliders and functions to test collisions

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//RectX and rectY are the center of the rectangle
function CollRect( centerX, centerY, width, height ) {
    this.centerX = centerX;
    this.centerY = centerY;
    this.width = width;
    this.height = height;
    this.setPos = function( x, y ) {
        this.centerX = x;
        this.centerY = y;
    }
}

function CollCircle( centerX, centerY, radius ) {
    this.centerX = centerX;
    this.centerY = centerY;
    this.radius = radius;
    this.setPos = function( x, y ) {
        this.centerX = x;
        this.centerY = y;
    }
}

//RectX and rectY are the center of the rectangle, width and height stretch outwards
function isCircleInRect( inputCircle, inputRect ) {
    var circleDistX = Math.abs( inputCircle.centerX - inputRect.centerX );
    var circleDistY = Math.abs( inputCircle.centerY - inputRect.centerY );
    if ( circleDistX > ( inputRect.width / 2.0 + inputCircle.radius ) ) { return false; }
    
    if ( circleDistY > ( inputRect.height / 2.0 + inputCircle.radius ) ) { return false; }
    
    if ( circleDistX <= ( inputRect.width / 2.0 )) { return true; }
    if ( circleDistY <= ( inputRect.height / 2.0 )) { return true; }
    
    var cornerDist = Math.pow(( circleDistX - inputRect.width / 2.0 ), 2 ) + Math.pow(( circleDistY - inputRect.height / 2.0 ), 2 );
    
    return (cornerDist <= ( Math.pow( inputCircle.radius, 2 ) ));
}
/*
//RectX and rectY are the center of the rectangle, width and height stretch outwards
function isCircleInRect( circleX, circleY, circleRadius, rectX, rectY, rectWidth, rectHeight ) {
    var circleDistX = Math.abs( circleX - rectX );
    var circleDistY = Math.abs( circleY - rectY );
    
    if ( circleDistX > ( rectWidth / 2.0 + circleRadius ) ) { return false; }
    if ( circleDistY > ( rectHeight / 2.0 + circleRadius ) ) { return false; }

    if ( circleDistX <= ( rectWidth / 2.0 )) { return true; }
    if ( circleDistY <= ( rectHeight / 2.0 )) { return true; }
    
    var cornerDist = Math.pow(( circleDistX - rectWidth / 2.0 ), 2 ) + Math.pow(( circleDistY - rectHeight / 2.0 ), 2 );
    
    return (cornerDist <= ( Math.pow( circleRadius, 2 ) ));
}
*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
