var res = {
    HelloWorld_png : "res/ground.png",
    Background_img: "res/WoodFloor.png",
    
    Background_img1: "res/RockFloor.png",
    Background_img2: "res/WoodFloor.png",
    Background_img3: "res/TileFloor.png",
    BackgroundCover1_img: "res/DustOverlay.png",
    
    BlankOverlay_png: "res/blank.png",
    
    dust0_png: "res/FloorDust1.png",
    dust1_png: "res/FloorDust2.png",
    dust2_png: "res/FloorDust3.png",
    
    Pan0_png: "res/Pan/pan0.png",
    Pan1_png: "res/Pan/pan1.png",
    Pan2_png: "res/Pan/pan2.png",
    Pan3_png: "res/Pan/pan3.png",
    Pan4_png: "res/Pan/pan4.png",
    Pan5_png: "res/Pan/pan5.png",
    
    CloseNormal_png : "res/CloseNormal.png",
    CloseSelected_png : "res/CloseSelected.png",
    Dustpan_png: "res/Dustpan.png",
    Broom_png: "res/broom.png",
    Broom_plist: "res/broom.plist",
    BroomIdle_png:"res/BroomIdle.png",
    DustPuff_png: "res/NewPoof.png",
    DustPuff_plist: "res/NewPoof.plist",
    Hat_png : "res/dust2.png",
    S1_png : "res/fall1.png",
    S2_png : "res/fall2.png",
    S3_png : "res/fall3.png",
    S4_png : "res/fall4.png",
    S5_png : "res/fall5.png",
    S6_png : "res/fall6.png",
    S7_png : "res/fall7.png",
    broom_down_wav : "res/broomDown.wav",
    broom_up_wav : "res/broomUp.wav",
    broom_move_wav : "res/broomMove.wav",
    sweep_sweep_wma : "res/Sweep_Sweep_Play.wav",
    intro1_png : "res/intro1.png",

    waterDrop_wav: "res/waterDrop.wav",
    
    intro_png : "res/ZenDenScreen2.png"



};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}