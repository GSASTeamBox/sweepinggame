

var mainCover = cc.Sprite.extend({
    alpha: 1,
    ctor:function() {
        this._super(res.BlankOverlay_png);
        //this._super(res.CloseSelected_png);
        this.setScale(1);
        this.setPosition( cc.winSize.width / 2, cc.winSize.height / 2);
        this.scheduleUpdate();//Do this so the update method gets called
    },
    
    update:function(dt) {
        this.alpha -= this.alpha * 0.03;
        this.opacity = this.alpha * 255;
    }

});

var FlashLayer = cc.Layer.extend({
    mainCover:null,
    ctor:function () {

        this._super();
        this.mainCover = new mainCover();
        this.addChild( this.mainCover );

    },
    
    doFlash:function() {
        this.mainCover.alpha = 1;
           
    }

});
